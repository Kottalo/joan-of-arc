extends "res://Scripts/SceneManager.gd"

onready var Background = $Origin/Background
onready var Foreground = $Origin/Foreground
var TargetPoint
var Crystal
onready var _camera = $Origin/Camera2D

func _ready():
	TargetPoint = $Origin/Joan/TargetPoint
	Crystal = TargetPoint.get_node("Crystal")
	Main.controllable = false
	Main.Joan.Whisper("JOAN")
	yield(self, "next")
	Main.Joan.EndWhisper()
	Main.controllable = true
	$Origin/Spot1.show()
	yield(self, "next")
	yield(get_tree().create_timer(2.0), "timeout")
	Main.Joan.Whisper("JOAN")
	yield(self, "next")
	Main.Joan.EndWhisper()
	Main.controllable = true
	$Origin/Spot2.show()
	yield(self, "next")
	yield(get_tree().create_timer(2.0), "timeout")
	$Tween.interpolate_property(Foreground, "modulate:a", Foreground.modulate.a, 0, 4,Tween.TRANS_BACK,Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	Main.controllable = false
	Main.Joan.Play("Idle")
	yield(get_tree().create_timer(2.0), "timeout")
	Main.Joan.Play("Back")
	yield(get_tree().create_timer(0.2), "timeout")
	$Tween.interpolate_property(_camera, "zoom", _camera.zoom, Vector2(0.7, 0.7), 4, Tween.TRANS_BACK, Tween.EASE_IN)
	$Tween.interpolate_property(_camera, "global_position", _camera.global_position, Main.Joan.get_node("TargetPoint").global_position, 4, Tween.TRANS_BACK, Tween.EASE_OUT)
	$Tween.interpolate_property(Background, "global_position", Background.global_position, Main.Joan.get_node("TargetPoint").global_position, 4, Tween.TRANS_BACK, Tween.EASE_OUT)
	$Tween.interpolate_property(Background, "scale", Background.scale, Vector2(1.2,1.2), 4, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	yield(get_tree().create_timer(0.2), "timeout")
	Crystal.get_node("Floating").cur_pos = TargetPoint.global_position
	$Tween.interpolate_property(Crystal, "self_modulate:a", 0, 1, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	
func show_dialog1():
	Main.Joan.Whisper("JOAN")
	yield(self, "next")
	Main.Joan.EndWhisper()

func vanish():
	$Tween.interpolate_property()