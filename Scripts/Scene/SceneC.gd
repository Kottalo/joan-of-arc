extends "res://Scripts/SceneManager.gd"

func _ready():
	Main.controllable = true

func _process(delta):
	if Joan.global_position.x >= $Origin/Background/Church.global_position.x:
		if Main.notes.find(2) == -1:
			Main.notes.append(2)
			Note.switch_to_last()
			get_tree().get_current_scene().get_node("Canvas/UI/NoteButton").visible = len(Main.notes)
			set_process(false)