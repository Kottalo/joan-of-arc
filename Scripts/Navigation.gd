extends Navigation2D

var Joan

# Called when the node enters the scene tree for the first time.
func _ready():
	Joan = get_tree().get_current_scene().get_node("Origin/Joan")
	pass # Replace with function body.

func _process(delta):
	var mouse_pos = get_global_mouse_position()
	if Joan.global_position.distance_to(mouse_pos) > 20:
		move_to_position(mouse_pos)

func _unhandled_input(event):
	set_process(Input.is_mouse_button_pressed(BUTTON_LEFT))
		

func move_to_position(target):
	var new_path = get_simple_path(Joan.global_position, target)
	Joan.path = new_path
	Joan.target_point = new_path[new_path.size() - 1]