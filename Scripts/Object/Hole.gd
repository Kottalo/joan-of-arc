extends "res://Scripts/Interactable.gd"

func interact():
	match Main.using_item:
		"Flower":
			Items.DropItem("Flower")
			Main.Joan.Say(0, "Yea")
		null:
			Main.Joan.Say(1, "Omg")
			
	yield(Main.Scene, "next")
	Main.Joan.EndDialog()
	