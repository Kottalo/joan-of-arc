extends Node2D

var tween

func _ready():
	tween = Tween.new()
	add_child(tween)
	rotation_degrees = rand_range(0, 359)

func interact():
	Jump()

func Jump():
	var rad = rand_range(0, PI)
	var dist = rand_range(100, 120)

	tween.interpolate_property(self, "global_position", global_position, Vector2(global_position.x + cos(rad) * dist, global_position.y + sin(rad) * dist), 0.1,Tween.TRANS_BOUNCE,Tween.EASE_IN)
	tween.start()
	rotation_degrees = rad2deg(rad + 90)
	$AnimSprite.frame = 1
	yield(tween, "tween_completed")
	$AnimSprite.frame = 0