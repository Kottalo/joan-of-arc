extends "res://Scripts/Interactable.gd"

onready var Scene = get_tree().get_current_scene()

func interact():
	if Main.using_item != null:
		if Main.using_item.find(name) != -1:
			
			Scene.score += 1
			$Sprite.show()
			if Scene.score == 4:
				Main.bridge_fixed = true
				Scene.get_node("Origin/BrokenBridge/ToggleObject").set_toggle(true)
				