tool
extends Camera2D

func _ready():
	var bg : Sprite = get_tree().get_current_scene().get_node("Origin/Background")
	var bg_half_width = bg.get_texture().get_size().x / 2
	var new_size : float = bg_half_width * bg.scale.x
	var distance_x = global_position.x - bg.global_position.x
	limit_left = global_position.x - distance_x - new_size
	limit_right = global_position.x - distance_x + new_size

func _process(delta):
	if Engine.editor_hint:
		self.global_position.y = get_viewport().get_visible_rect().size.y / 2