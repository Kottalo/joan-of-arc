extends Panel

export(float) var padding_right = 40

var pad_x = 50
var pad_y = 30

var NPC
var Dialog

func _ready():
	NPC = get_parent().get_parent()
	Dialog = get_parent()
	

func SetText(text):
	$Label.set_text(text)
	
	var max_width = get_tree().get_current_scene().rect_size.x - rect_global_position.x - padding_right
	print($Label.rect_min_size.x)
	if $Label.rect_min_size.x > max_width:
		var width = $Label.rect_size.x
		var lines = max_width / width
		$Label.autowrap = true
		$Label.rect_size = Vector2(max_width, $Label.rect_size.y * lines)
	else:
		$Label.rect_size = $Label.get_minimum_size()
	rect_size = $Label.rect_size + Vector2(pad_x * 2, pad_y * 2)
	$Label.rect_position = Vector2(pad_x, pad_y)
	if NPC.global_position.x > Dialog.rect_global_position.x:
		rect_position = Vector2(-rect_size.x, 0)
		$TailLeft.hide()
	else:
		rect_position = Vector2.ZERO
		$TailRight.hide()