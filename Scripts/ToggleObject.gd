extends Node2D

var toggle setget set_toggle

func set_toggle(value):
	$Off.visible = !value
	$On.visible = value