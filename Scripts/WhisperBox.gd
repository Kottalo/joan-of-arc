extends Panel

var pad_x = 50
var pad_y = 30

func SetText(text):
	$Label.set_text(text)
	$Label.rect_size = $Label.get_minimum_size()
	rect_size = $Label.rect_size + Vector2(pad_x * 2, pad_y * 2)
	$Label.rect_position = Vector2(pad_x, pad_y)
	rect_global_position.x = get_tree().get_current_scene().rect_size.x / 2 - rect_size.x / 2
	