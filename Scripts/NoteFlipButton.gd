extends TextureButton

export(String) var command

var container

func _ready():
	connect("button_down", self, "command")
	container = $"../../ScrollContainer"

func command():
	container.emit_signal(command)