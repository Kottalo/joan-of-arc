extends Node2D

func _ready():
	visible = not name in Main.items

func interact():
	Main.items[name] = $Sprite.texture.resource_path
	get_tree().get_current_scene().get_node("Canvas/UI/Items").ListItems()
	_ready()