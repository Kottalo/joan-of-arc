extends Control

var dialog_gap = 20
var DialogBox = preload("res://Prefabs/DialogBox.tscn")
var detector

func ShowText(text):
	for dialog in get_children():
		dialog.modulate = Color(1,1,1,1)
		dialog.get_node("TailLeft").hide()
		dialog.get_node("TailRight").hide()
	
	if get_child_count() > 2:
		get_children()[0].queue_free()
	
	if get_child_count() > 0:
		get_children()[get_child_count()-2].modulate = Color(1,1,1,0.4)
		get_children()[get_child_count()-1].modulate = Color(1,1,1,0.7)

	detector = get_tree().get_current_scene().get_node("Canvas/ClickDetector")
	detector.show()
	var dialog_box = DialogBox.instance()
	add_child(dialog_box)
	dialog_box.SetText(text)

	for dialog in get_children():
		dialog.rect_position.y -= dialog_box.rect_size.y + dialog_gap

func ClearDialog():
	for child in get_children():
		child.queue_free()