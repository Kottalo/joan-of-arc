extends Node

signal next

onready var ClickDetector = $Canvas/ClickDetector
onready var tween : Tween = $Tween
onready var Note = $Canvas/UI/Note
onready var Joan = get_node("Origin/Joan")

var camera

func _ready():
	$Canvas/UI/NoteButton.visible = len(Main.notes)
	Main.Scene = get_tree().get_current_scene()
	Main.Joan = Main.Scene.get_node("Origin/Joan")
	camera = Main.Joan.get_node("Camera2D")
	pass
	
func Focus(target, cam_zoom=0.85, duration=1):
	tween.interpolate_property(camera, "zoom", camera.zoom, Vector2(cam_zoom, cam_zoom), duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(camera, "global_position", camera.global_position, target, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()

func CancelFocus(duration=1):
	tween.interpolate_property(camera, "global_position", camera.global_position, Vector2($"Origin/Joan".global_position.x, get_viewport().get_visible_rect().size.y / 2), duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(camera, "zoom", camera.zoom, Vector2(1,1), duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()