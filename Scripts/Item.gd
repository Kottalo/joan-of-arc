extends TextureRect

var outline_shader = preload("res://Resources/Shader/Outline.tres")

func SetOutline(value):
	if value:
		material = outline_shader
	else:
		material = null

func _on_Item_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		get_parent().ClearUsingItem()
		Main.using_item = name
		SetOutline(true)
	pass # Replace with function body.
