extends Control

func _gui_input(event):
	if event is InputEventMouseButton and event.pressed and Input.is_mouse_button_pressed(BUTTON_LEFT):
		visible = false
		Main.controllable = true

func switch_to_last():
	Main.controllable = false
	Main.cur_note = len(Main.notes) - 1
	$ScrollContainer.update_note()
	show()