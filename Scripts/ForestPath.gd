extends Node2D

onready var Scene = get_tree().get_current_scene()
onready var Joan = get_tree().get_current_scene().get_node("Origin/Joan")

func interact():
	Joan.Whisper("It's the way to forest")
	yield(Scene, "next")
	Joan.EndWhisper()