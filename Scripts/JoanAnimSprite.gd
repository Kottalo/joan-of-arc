extends AnimatedSprite

var offsets = {
	"Idle": Vector2(0.959, -259.289),
	"Back": Vector2(70.525, -315.518),
	"Walk_LF": Vector2(-18.332, -261.7),
	"Walk_LB": Vector2(-18.332, -261.7),
	"Walk_RF": Vector2(5.781, -261.7),
	"Walk_RB": Vector2(5.781, -261.7),
	"Push": Vector2(2.201,-282.404)
}

var Illusion

# Called when the node enters the scene tree for the first time.
func _ready():
	Illusion = $"../ReflectPoint/Illusion"
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func Play(anim):
	if animation != anim:
		offset = offsets[anim]
		set("animation", anim)

func _set(property, value):
	if property == "animation":
		if Illusion:
			Illusion.animation = value
			Illusion.frame = frame
			Illusion.offset = Vector2(offset.x, -offset.y)