extends TextureButton

onready var Game = get_parent().get_parent()

func _ready():
	connect("button_down", self, "_on_button_down")

func _on_button_down():
	Game.hide()
	Main.controllable = true