extends "res://Scripts/NPC.gd"

func _ready():
	pass

func interact():
	yield(get_tree().create_timer(0.2), "timeout")
	Joan.Play("Back")
	Scene.Focus($FocusPoint.global_position)
	match Main.timeline:
		0:
			Joan.Say(1,"WHERE_OTHERS")
			yield(Scene, "next")
			Joan.EndDialog()
			Say("THEY_HAVE")
			yield(Scene, "next")
			Say("LOOK")
			yield(Scene, "next")
			Say("THEY_MIGHT")
			yield(Scene, "next")
			Say("I_LL_FIX")
			yield(Scene, "next")
			EndDialog()
			Scene.CancelFocus(1)
			if not Main.papa_talk:
				Main.notes.append(1)
				Scene.Note.switch_to_last()
				get_tree().get_current_scene().get_node("Canvas/UI/NoteButton").visible = len(Main.notes)
				Main.papa_talk = true
		1:
			Joan.Say(1,"DOMINIC")
			yield(Scene, "next")
			Joan.EndDialog()
			Say("GOD_BLESS")
			yield(Scene, "next")
			EndDialog()