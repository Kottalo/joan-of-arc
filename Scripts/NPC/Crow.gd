extends "res://Scripts/NPC.gd"

var disturbed = 0

func interact():
	disturbed += 1
	Play("Caw")
	Say("CAW")
	yield(Scene, "next")
	Play("Idle")
	EndDialog()
	
	if disturbed > 2:
		Play("Fly")
		var tween : Tween = get_tree().get_current_scene().get_node("Tween")
		tween.interpolate_property(get_parent(), "unit_offset", 0, 1, 4, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.interpolate_property(self, "rotation_degrees", 163, 136, 4, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.start()
		Main.crow = false