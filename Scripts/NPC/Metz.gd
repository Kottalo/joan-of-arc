extends "res://Scripts/NPC.gd"

func interact():
	Say("JOAN_HELP")
	yield(Scene, "next")
	Joan.Say(1, "WHY_WHO")
	yield(Scene, "next")
	Joan.EndDialog()
	Say("CANT_TELL")
	yield(Scene, "next")
	Say("IM_HURT")
	yield(Scene, "next")
	Say("BUT")
	yield(Scene, "next")
	EndDialog()
	$Board3.interact()
	if Main.notes.find(6) == -1:
		Main.notes.append(6)
		Scene.Note.switch_to_last()