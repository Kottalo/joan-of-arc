extends "res://Scripts/NPC.gd"

func interact():
	Scene.Focus($FocusPoint.global_position)
	Say("LEAVE_ME")
	yield(Scene, "next")
	Say("GOD_DAMN")
	yield(Scene, "next")
	EndDialog()
	Scene.CancelFocus()
	if Main.notes.find(4) == -1:
		Main.notes.append(4)
		Scene.Note.switch_to_last()