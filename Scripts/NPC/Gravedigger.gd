extends "res://Scripts/NPC.gd"

onready var Woman = $"../Woman"
onready var Guillaume = $"../Guillaume"

func interact():
	Say("NO_ONE")
	yield(Scene, "next")
	Say("LIFE_AND")
	yield(Scene, "next")
	EndDialog()
	Woman.SaySpeech()
	yield(Scene, "next")
	Scene.Focus(Scene.get_node("Origin/FocusPoint").global_position)
	Guillaume.SaySpeech()
	yield(Scene, "next")
	Scene.CancelFocus()
	Joan.Whisper("I_HAVE")
	yield(Scene, "next")
	Joan.EndWhisper()
	if Main.notes.find(3) == -1:
		Main.notes.append(3)
		Scene.Note.switch_to_last()
		get_tree().get_current_scene().get_node("Canvas/UI/NoteButton").visible = len(Main.notes)
		Main.timeline += 1