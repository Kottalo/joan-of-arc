extends "res://Scripts/NPC.gd"

func interact():
	match Main.Hauviette_phase:
		0:
			Joan.Say(1, "WHAT_ARE")
			yield(Scene, "next")
			Joan.EndDialog()
			Say("NO_ONE_TELLS")
			yield(Scene, "next")
			Joan.Say(1, "WE_BELONG")
			yield(Scene, "next")
			Say("IS_THAT")
			yield(Scene, "next")
			Joan.EndDialog()
			EndDialog()
			Main.Hauviette_phase = 1
		1:
			var tween = Tween.new()
			add_child(tween)
			var AnimSprite : AnimatedSprite = Main.Joan.get_node("AnimSprite")
			AnimSprite.Play("Push")
			tween.interpolate_property(get_parent(), "unit_offset", 0, 1, 0.2, Tween.TRANS_BOUNCE, Tween.EASE_IN)
			tween.start()
			tween.interpolate_property(Scene.get_node("Origin/Path2D2/PathFollow2D"), "unit_offset", 0, 1, 5, Tween.TRANS_SINE, Tween.EASE_IN)
			tween.start()
			$AnimatedSprite.play("Swim")
			$AnimatedSprite.offset.y = 200
			yield(AnimSprite, "animation_finished")
			AnimSprite.Play("Idle")
			Joan.Say(1, "DOES_THAT")
			yield(Scene, "next")
			Joan.EndDialog()