extends ProgressBar

func _gui_input(event):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		value = clamp(event.position.x / rect_min_size.x, 0, 1) * 100