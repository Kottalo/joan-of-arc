extends Node2D

var Scene
var HorizonA
var HorizonB
var WhisperBox
var speed = 400
var target_point : Vector2
var path = PoolVector2Array() setget set_path

# Called when the node enters the scene tree for the first time.
func _ready():
	Scene = get_tree().get_current_scene()
	HorizonA = Scene.get_node("Origin/HorizonA")
	HorizonB = Scene.get_node("Origin/HorizonB")
	WhisperBox = Scene.get_node("Canvas/WhisperBox")
	update_scale()
	set_process(false)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var move_distance = speed * delta
	if Main.controllable: move_along_path(move_distance)
	update_scale()

func move_along_path(distance : float):
	var start_point = global_position
	for i in range(path.size()):
		var distance_to_next = start_point.distance_to(path[0])
		if distance <= distance_to_next and distance >= 0.0:
			var anim = "Walk_"
			
			if path[0].x > global_position.x:
				anim+="R"
			else:
				anim+="L"
			
			if global_position.angle_to_point(path[0]) >= 0.785 and global_position.angle_to_point(path[0]) <= 2.356:
				anim+="B"
			else:
				anim+="F"
			
			$AnimSprite.Play(anim)
			global_position = start_point.linear_interpolate(path[0], distance / distance_to_next)
			break
		
		distance -= distance_to_next
		start_point = path[0]
		path.remove(0)
	
	if path.size() < 1:
		global_position = target_point
		$AnimSprite.Play("Idle")
		set_process(false)

func set_path(value : PoolVector2Array):
	path = value
	
	if value.size() == 0:
		return
	set_process(true)

func update_scale():
	z_index = global_position.y
	var _scale = (z_index - HorizonA.global_position.y) / HorizonB.global_position.y
	var scale_value = lerp(HorizonA.scale_to, HorizonB.scale_to, _scale)
	scale = Vector2(scale_value, scale_value)

func Say(lr, text):
	Scene.ClickDetector.show()
	if lr:
		$DialogRight.ShowText(tr(text))
	else:
		$DialogLeft.ShowText(tr(text))

func Whisper(text):
	Scene.ClickDetector.show()
	WhisperBox.SetText(tr(text))
	WhisperBox.show()

func EndWhisper():
	Scene.ClickDetector.hide()
	WhisperBox.hide()

func EndDialog():
	Scene.ClickDetector.hide()
	$DialogLeft.ClearDialog()
	$DialogRight.ClearDialog()

func Play(anim):
	$AnimSprite.Play(anim)