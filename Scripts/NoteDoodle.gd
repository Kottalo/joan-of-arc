extends TextureRect

export(Dictionary) var locale_pos = {
	"en" : Vector2(),
	"zh" : Vector2()
}

func _ready():
	Main.connect("language_changed", self, "change_pos")
	change_pos()

func change_pos():
	rect_position = locale_pos[TranslationServer.get_locale()]