extends Node2D

var tween1
var tween2
export(Vector2) var radius = Vector2(5, 8)
var cur_pos

func _ready():
	cur_pos = get_parent().global_position
	tween1 = Tween.new()
	tween2 = Tween.new()
	add_child(tween1)
	add_child(tween2)
	tween1.repeat = true
	tween2.repeat = true
	tween1.interpolate_method(self, "float_x", 0, PI * 2, 4, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween2.interpolate_method(self, "float_y", 0, PI * 2, 2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween1.start()
	tween2.start()

func float_x(radian):
	get_parent().global_position.x = cos(radian) * radius.x + cur_pos.x

func float_y(radian):
	get_parent().global_position.y = sin(radian) * radius.y + cur_pos.y