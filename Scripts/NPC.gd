extends Node

var Scene
var Joan

func _ready():
	Scene = get_tree().get_current_scene()
	Joan = Scene.get_node("Origin/Joan")

func Say(text):
	Main.controllable = false
	Scene.ClickDetector.show()
	$Dialog.ShowText(tr(text))

func Play(anim):
	$AnimatedSprite.play(anim)

func EndDialog():
	Scene.ClickDetector.hide()
	$Dialog.ClearDialog()
	yield(get_tree().create_timer(0.2), "timeout")
	Main.controllable = true

func GoToScene(name):
	Main.last_scene = Scene.name
	get_tree().change_scene("res://Scenes/"+name+".tscn")
	Main.controllable = false