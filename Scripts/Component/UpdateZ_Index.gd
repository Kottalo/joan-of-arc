extends Node2D

export(bool) var Enabled = true
export(bool) var Update

func _ready():
	update()

func _process(delta):
	if Update:
		update()

func update():
	if Enabled:
		get_parent().z_index = get_parent().global_position.y