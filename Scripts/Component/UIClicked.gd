extends Control

func _ready():
	get_parent().connect("gui_input", self, "_gui_input")
	get_parent().connect("mouse_entered", self, "_mouse_entered")
	get_parent().connect("mouse_exited", self, "reset_modulate")

func _gui_input(event):
	if event is InputEventMouseButton and event.is_pressed():
		get_parent().action()
		get_parent().modulate = Color(.5,.5,.5)
	else:
		reset_modulate()

func _mouse_entered():
	get_parent().modulate = Color(.5,.5,.5)

func reset_modulate():
	get_parent().modulate = Color(1,1,1)
