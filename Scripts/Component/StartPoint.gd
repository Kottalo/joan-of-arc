extends Node2D

export(String) var scene_name
var Scene

func _ready():
	Scene = get_tree().get_current_scene()
	if Main.last_scene == scene_name:
		Scene.get_node("Origin/Joan").global_position = global_position