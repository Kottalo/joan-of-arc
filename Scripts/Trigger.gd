extends Area2D

signal go

export(bool) var ClickOnly = false

var enter = false
onready var Scene = get_tree().get_current_scene()
var Joan
var Nav

func _ready():
	Joan = Scene.get_node("Origin/Joan")
	Nav = Scene.get_node("Nav")
	
	connect("input_event", self, "_on_Portal_input_event")
	connect("mouse_entered", self, "_mouse_entered")
	connect("mouse_exited", self, "_mouse_exited")
	connect("go", get_parent(), "interact")
	set_process(false)
	pass

func _process(delta):
	if $Position2D.global_position.distance_to(Joan.global_position) == 0 and enter:
		emit_signal("go")
		get_tree().get_current_scene().get_node("Canvas/UI/Items").ClearUsingItem()
		set_process(false)

func _on_Portal_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and !event.pressed and ClickOnly:
			emit_signal("go")
		if event.button_index == BUTTON_LEFT and !event.pressed and Main.controllable:
			enter = true
			$Icon.hide()
			$Label.hide()
			Nav.move_to_position($Position2D.global_position)
			set_process(true)
	pass # Replace with function body.

func _mouse_entered():
	$Icon.show()
	if !ClickOnly:
		$Label.show()
	
func _mouse_exited():
	$Icon.hide()
	if !ClickOnly:
		$Label.hide()
