extends GridContainer

var item_prefab = preload("res://Prefabs/Item.tscn")

func _ready():
	ListItems()

func ListItems():
	for child in get_children():
		child.queue_free()
	
	for item in Main.items:
		var Item = item_prefab.instance()
		Item.name = item
		print(Main.items[item])
		Item.set_texture(load(Main.items[item]))
		add_child(Item)

func DropItem(name):
	Main.items.erase(name)
	ListItems()

func ClearUsingItem():
	Main.using_item = null
	for child in get_children():
		child.SetOutline(false)