extends Sprite

func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_CONTROL:
			show()
		if !Input.is_key_pressed(KEY_CONTROL):
			hide()
		