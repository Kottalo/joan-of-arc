extends TextureButton

export(String) var target

func _ready():
	connect("button_down", self, "_on_button_down")

func _on_button_down():
	get_node(target).visible = !get_node(target).visible
	
func _on_button_up():
	get_node(target).visible = !get_node(target).visible
