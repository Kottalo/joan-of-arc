extends Label

func _ready():
	get_parent().connect("gui_input", self, "_gui_input")

func _gui_input(event):
	if event is InputEventMouseButton and event.is_pressed():
		get_parent().action()
		get_parent().modulate = Color(.5,.5,.5)
	else:
		get_parent().modulate = Color(1,1,1)