extends Label

export(String) var tr_id

func _ready():
	Main.connect("language_changed", self, "change_language")
#	change_language()

func change_language():
	set_text(tr(tr_id))