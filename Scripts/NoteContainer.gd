extends ScrollContainer

signal go_left
signal go_right

func _ready():
	connect("go_left", self, "go_left")
	connect("go_right", self, "go_right")

func go_left():
	if Main.cur_note > 0:
		Main.cur_note -= 1
	update_note()

func go_right():
	if Main.cur_note < len(Main.notes) - 1:
		Main.cur_note += 1
	update_note()

func update_note():
	for child in get_children():
		child.visible = false
		child.rect_min_size.y = 0
	
	var cur_child = get_node("Note_" + str(Main.notes[Main.cur_note]))
	cur_child.visible = true
	cur_child.rect_min_size.y = cur_child.get_node("TextureRect").rect_position.y + cur_child.get_node("TextureRect").rect_size.y
	cur_child.change_language()