extends OptionButton

var locales = [
	"en",
	"zh"
]

func _ready():
	connect("item_selected", self, "_item_selected")
	add_item("English")
	add_item("中文")
	select(Main.locale)

func _item_selected(id):
	Main.locale = id
	TranslationServer.set_locale(locales[id])
	Main.emit_signal("language_changed")